class AlbumsController < ApplicationController
  def index
    @albums = Album.all
  end

  def show
    @album = Album.find(params[:id])
    @tracks = Track.find_by album_id: 'a2.id'
  end
  
  
end
